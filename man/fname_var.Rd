% Generated by roxygen2: do not edit by hand
% Please edit documentation in R/get_eset_specific_var.R
\name{fname_var}
\alias{fname_var}
\alias{fname_values}
\alias{get_gene_symbol_var}
\alias{get_gene_symbol_values}
\title{Get fname var/values}
\usage{
fname_var(object)

fname_values(object)

get_gene_symbol_var(object)

get_gene_symbol_values(object)
}
\arguments{
\item{object}{eset}
}
\value{
fvar with gene symbols
}
\description{
Get fname var/values
}
\examples{
library(magrittr)
if (require(atkin.2014)){
   atkin.2014::soma \%>\% autonomics.import::fname_var()
   atkin.2014::soma \%>\% autonomics.import::fname_values()
}
}
