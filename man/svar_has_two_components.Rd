% Generated by roxygen2: do not edit by hand
% Please edit documentation in R/svar_has_two_components.R
\name{svar_has_two_components}
\alias{svar_has_two_components}
\alias{subgroup_has_two_components}
\title{Does svar have two components?}
\usage{
svar_has_two_components(object, svar)

subgroup_has_two_components(object)
}
\arguments{
\item{object}{SummarizedExperiment, eSet, or EList}

\item{svar}{sample var}
}
\value{
logical
}
\description{
Does svar have two components?
}
\examples{
require(magrittr)
if (require(subramanian.2016)){
   object <- subramanian.2016::metabolon
   object \%>\% autonomics.import::svar_has_two_components('subgroup')
   object \%>\% autonomics.import::subgroup_has_two_components()
}
if (require(halama.2016)){
   halama.2016::cell.metabolites \%>\%
      autonomics.import::subgroup_has_two_components()
}
if (require(billing.differentiation.data)){
   billing.differentiation.data::protein.ratios \%>\%
      autonomics.import::subgroup_has_two_components()
}
}
