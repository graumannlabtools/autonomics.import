## Purpose ##

Import omics data an [autonomics ](https://bitbucket.org/graumannlabtools/autonomics)-ready eset.    

## Install ##
    install.packages('devtools')
    devtools::install_bitbucket('graumannlabtools/autonomics.import')